package com.dansardev.mimascotaideal;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {
    Button btnAddMascota;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.Theme_MiMascotaIdeal); //para mostrar splashscreen
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Retardo splash
        try {
            Thread.sleep(2000);

        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        //Botones
        btnAddMascota = findViewById(R.id.btnAgregarMascota);
        btnAddMascota.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, MascotasListar.class);
                startActivity(intent);
            }
        });

    }
}